$(Document).ready(function() {
    
    $('.js--first-section').waypoint(function(direction) {
        if (direction == "down") {
            $('nav').addClass('sticky');
        } else {
            $('nav').removeClass('sticky');
        }
    }, {
        offset: '140px;'
    });
    
    $('.js--introduce').click(function (){
       $('html, body').animate({scrollTop: $('.js--first-section').offset().top}, 1000)
    });
    
    $('.js--informations').click(function (){
       $('html, body').animate({scrollTop: $('.js--informations-section').offset().top}, 1000)
    });
    
    $('.js--images').click(function (){
       $('html, body').animate({scrollTop: $('.js--images-section').offset().top}, 1000)
    });
    
    $('.js--friends').click(function (){
       $('html, body').animate({scrollTop: $('.js--friends-section').offset().top}, 1000)
    });
    
});